export async function getCroppedImg(imageSrc, pixelCrop) {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const image = new Image();
  
    return new Promise((resolve, reject) => {
      image.onload = () => {
        const crop = pixelCrop;
        canvas.width = crop.width;
        canvas.height = crop.height;
  
        ctx.drawImage(
          image,
          crop.x,
          crop.y,
          crop.width,
          crop.height,
          0,
          0,
          crop.width,
          crop.height
        );
  
        canvas.toBlob((blob) => {
          resolve(blob);
        }, 'image/jpeg');
      };
  
      image.onerror = reject;
      image.src = imageSrc;
    });
  }