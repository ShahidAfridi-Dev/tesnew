import React, { createContext, useContext, useState } from 'react';

// Set default context values in the argument to createContext
const TokenContext = createContext({
  token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqYm9faWQiOjI0OSwidXNlcl9pZCI6MTY0LCJvdHBfdmVyaWZpY2F0aW9uIjowLCJreWNfdmVyaWZpY2F0aW9uIjoxLCJhY2NlcHRfcG9saWN5IjoxLCJpYXQiOjE3MDA3MzIyMDV9.N5ZS3Pi7xoxs4SH62-G7FArrid0xio5AQy3bssmcbzo', // Default token value
  setToken: () => {}, // Placeholder function
  jboId: 201, // Default JBO ID value
  setJboId: () => {} // Placeholder function
});

export const TokenProvider = ({ children }) => {
  const [token, setToken] = useState('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqYm9faWQiOjI0OSwidXNlcl9pZCI6MTY0LCJvdHBfdmVyaWZpY2F0aW9uIjowLCJreWNfdmVyaWZpY2F0aW9uIjoxLCJhY2NlcHRfcG9saWN5IjoxLCJpYXQiOjE3MDA3MzIyMDV9.N5ZS3Pi7xoxs4SH62-G7FArrid0xio5AQy3bssmcbzo');
  const [jboId, setJboId] = useState(201);

  return (
    <TokenContext.Provider value={{ token, setToken, jboId, setJboId }}>
      {children}
    </TokenContext.Provider>
  );
};

export const useToken = () => {
  const context = useContext(TokenContext);
  
  if (!context) {
    throw new Error('useToken must be used within a TokenProvider');
  }
  
  return context;
};
