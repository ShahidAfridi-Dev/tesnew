import React, { useState } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import useTokenFromURL from "../Context/useTokenFromURL";
import useAxios from "../Axios/useAxios";
// Import BASE_URL from config.js
import { BASE_URL } from '../config';

const REACT_APP_BASE_URL = BASE_URL;
const ThemeCard = ({ themeName, themeImage, isSelected, onClick }) => {
  return (
    <div
      className={`w-1/4 h-80 m-4 border-2 rounded-lg cursor-pointer transition-all duration-300 shadow-md transform hover:scale-105 hover:rotate-3 ${
        isSelected ? "border-blue-500" : "border-gray-300"
      }`}
      onClick={onClick}
    >
      <div
        className="w-full h-2/3 rounded-t-lg bg-contain bg-center flex items-end justify-center"
        style={{ backgroundImage: `url(${themeImage})` }}
      >
        <img
          src={themeImage}
          alt={themeName}
          className="max-h-full max-w-full object-contain p-4"
        />
      </div>
      <div className="w-full h-1/3 p-4 text-center">
        <p className="font-semibold text-lg">{themeName}</p>
      </div>
    </div>
  );
};

const ThemeSelector = ({ onThemeSelected }) => {
  useTokenFromURL();
  useAxios();
  const [selectedTheme, setSelectedTheme] = useState(null);

  const themes = [
    {
      name: "Antique",
      image:
        "https://www.shopatblu.com/wp-content/uploads/2023/01/shopatblu-vintage-jewelry-project-picture-frames.jpg",
    },
    {
      name: "Contemporary",
      image:
        "https://i.ebayimg.com/00/s/NzcwWDExNTU=/z/2PEAAOSwYV9e2gxl/$_57.JPG?set_id=8800005007",
    },
    {
      name: "Modern",
      image:
        "https://avatars.mds.yandex.net/get-altay/4581272/2a0000017ac4b705384530dc5eea1f0445b1/XXL",
    },
  ];
const TokenValid = async ()=>{
  
}
  const saveTheme = async () => {
    if (selectedTheme === null) {
      Swal.fire({
        icon: "warning",
        title: "Oops...",
        text: "Please select a theme before saving.",
        timer: 2000,
      });
      return;
    }

    try {
      const response = await axios.post(
        `${REACT_APP_BASE_URL}/theme-manager`,

        {
          theme_name: themes[selectedTheme].name,
          selected_theme: themes[selectedTheme].name ? 1 : 0,
        }
      );

      if (response.status === 201) {
        onThemeSelected(); // Notify parent that a theme has been selected
        Swal.fire({
          icon: "success",
          title: "Success",
          text: "Theme saved successfully!",
          timer: 2000,
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Failed to save theme.",
          timer: 2000,
        });
      }
    } catch (error) {
      console.error(error);
      if ((error.response.data.message == "User_id already exist")) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Already selected the theme. Use Update Option",
          timer: 2000,
        });
      }
    }
  };

  //   const updateTheme = async () => {
  //     if (selectedTheme === null) {
  //       Swal.fire({
  //         icon: "warning",
  //         title: "Oops...",
  //         text: "Please select a theme before updating.",
  //         timer: 2000,
  //       });
  //       return;
  //     }
  //     const url = `http://192.168.1.187:4000/theme-manager/${153}`; // Assuming 125 is the user_id

  //     try {
  //       const response = await axios.patch(url, {
  //         theme_name: themes[selectedTheme].name,
  //       });

  //       if (response.status === 200) {
  //         Swal.fire({
  //           icon: "success",
  //           title: "Success",
  //           text: "Theme updated successfully!",
  //           timer: 2000,
  //         });
  //       } else {
  //         Swal.fire({
  //           icon: "error",
  //           title: "Oops...",
  //           text: "Failed to update theme.",
  //           timer: 2000,
  //         });
  //       }
  //     } catch (error) {
  //       console.error(error);
  //       Swal.fire({
  //         icon: "error",
  //         title: "Oops...",
  //         text: "An error occurred while updating the theme.",
  //         timer: 2000,
  //       });
  //     }
  //   };

  return (
    <div className="min-h-screen bg-gray-100">
      <div className="container mx-auto py-16">
        <h1 className="text-center text-4xl mb-8">Select a theme</h1>
        <div className="flex justify-center items-start">
          {themes.map((theme, index) => (
            <ThemeCard
              key={index}
              themeName={theme.name}
              themeImage={theme.image}
              isSelected={selectedTheme === index}
              onClick={() => setSelectedTheme(index)}
            />
          ))}
        </div>
        <div className="flex justify-center mt-8">
          <button
            className="px-6 py-3 mr-3 text-white text-lg rounded-md transform hover:scale-105 transition-transform duration-200 ease-in-out shadow-lg hover:shadow-2xl bg-gradient-to-r from-blue-500 to-blue-700"
            onClick={saveTheme}
          >
            Save Theme
          </button>
          {/* {selectedTheme !== null && (
            <button
              className="px-6 py-3 text-white text-lg rounded-md transform hover:scale-105 transition-transform duration-200 ease-in-out shadow-lg hover:shadow-2xl bg-gradient-to-r from-green-500 to-green-700"
              onClick={updateTheme}
            >
              Update Theme
            </button>
          )} */}
        </div>
      </div>
    </div>
  );
};

export default ThemeSelector;
