import React, { useState, useEffect } from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Gallery from "../Gallery";
import { FiUpload } from "react-icons/fi";
import Swal from "sweetalert2";
import { AiFillDelete, AiOutlineClose } from "react-icons/ai";
import { FaRegHeart, FaShoppingCart, FaUser } from "react-icons/fa";
import { BiSearch } from "react-icons/bi";
import axios from "axios";
import Modal from "react-modal";
import { BASE_URL } from '../../config';
import useAxios from "../../Axios/useAxios";
import useTokenFromURL from "../../Context/useTokenFromURL";
// Base URL from environment variables. It's used for API calls.
const REACT_APP_BASE_URL = BASE_URL;

/**
 * HeaderCustomization component provides UI & functionality for customizing the header.
 * @param {object} props - Component props
 * @param {string} props.activeAccordion - The currently active accordion name.
 * @param {function} props.onToggle - Callback to toggle the accordion state.
 * @param {boolean} props.IsSavedInputs - Indicates if the inputs were saved.
 */
const HeaderCustomization = ({ activeAccordion, onToggle, IsSavedInputs }) => {
  // Custom hook to get token from URL (not shown in the provided code).
  useTokenFromURL();
  // Custom hook to set up Axios (not shown in the provided code).
  useAxios();

  // State variables declaration
  const [isImageModalOpen, setIsImageModalOpen] = useState(false); // Controls the visibility of the image modal
  const [selectedHeader, setSelectedHeader] = useState("header1"); // Stores the type of header selected by user
  const [dataSubmitted, setDataSubmitted] = useState(false); // Indicates if data has been submitted already
  const [image, setImage] = useState(null); // Holds the URL or data of the selected image
  const [errors, setErrors] = useState({}); // Holds any validation errors

  // Common CSS classes for icons and input elements
  const commonIconClasses = "text-2xl";
  const commonInputClasses = "bg-transparent ml-2 focus:outline-none";

  /**
   * Fetches the title bar data from API and updates local state accordingly.
   */
  const fetchTitleBarData = async () => {
    try {
      const response = await axios.get(`${REACT_APP_BASE_URL}/title-bar`);
      if (response.data && response.data.length > 0) {
        const fetchedData = response.data[0];
        const submitValidate = fetchedData.logo_url !== "" ? true : false;
        setDataSubmitted(submitValidate);
        // Updates the selectedHeader state based on the title_bar_type from fetched data
        if (fetchedData.title_bar_type === 1) {
          setSelectedHeader("header1");
        } else if (fetchedData.title_bar_type === 2) {
          setSelectedHeader("header2");
        }

        // Updates the image state with the logo_url from fetched data
        setImage(fetchedData.logo_url);
      }
    } catch (error) {
      console.error("Error fetching title bar data:", error);
      Swal.fire(
        "Error!",
        "An error occurred while fetching the data.",
        "error"
      );
    }
  };
  // React effect to fetch title bar data on component mount
  useEffect(() => {
    fetchTitleBarData();
  }, []);

  /**
   * Handles the form submission to update or save the title bar data.
   */
  const handleSubmit = async () => {
    // Dynamic payload based on the component's states
    const payload = {
      title_bar_type: selectedHeader === "header1" ? 1 : 2, // Adjust as per your needs
      logo_url: image, // Use the selected image or a default one
    };

    try {
      const response = await axios.patch(
        `${REACT_APP_BASE_URL}/title-bar`,
        payload
      );
      // Handle the response as needed
      if (dataSubmitted) {
        Swal.fire({
          title: "Success!",
          text: "Datas Updated successfully.",
          icon: "success",
          timer: 3000,
          showConfirmButton: false,
        });
      } else {
        Swal.fire({
          title: "Success!",
          text: "Datas saved successfully.",
          icon: "success",
          timer: 3000,
          showConfirmButton: false,
        });
      }
      fetchTitleBarData();
    } catch (error) {
      // Handle the error as needed
      Swal.fire(
        "Error!",
        "An error occurred while updating the data.",
        "error"
      );
    }
  };
  /**
   * Validates the fields and updates the errors state.
   * @returns {boolean} - Returns true if no validation errors, false otherwise.
   */
  const validateFields = () => {
    let validationErrors = {};
    // Check if image is provided
    if (!image) {
      validationErrors.image = "Image is required.";
    }

    setErrors(validationErrors);

    return Object.keys(validationErrors).length === 0; // returns true if no errors, false otherwise
  };

  // React effect to validate fields whenever the image state changes
  useEffect(() => {
    validateFields();
  }, [image]);

  /**
   * Handles the accordion change to toggle between open and close state.
   */
  const handleAccordionChange = () => {
    if (activeAccordion === "HeaderAccordian") {
      onToggle && onToggle(null); // close the current accordion if it's 'AuthAccordian'.
    } else {
      onToggle && onToggle("HeaderAccordian"); // open the 'AuthAccordian' if another accordion is currently active.
    }
  };
  return (
    <div>
      {/* header accordian Starts */}
      <Accordion
        expanded={activeAccordion === "HeaderAccordian"}
        onChange={handleAccordionChange}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography style={{ fontFamily: "poppins" }}>
            Choose Header
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="p-10">
            <div>
              {/* Your modal starts here */}
              <Modal
        isOpen={isImageModalOpen}
        onRequestClose={() => setIsImageModalOpen(false)}
        style={{
          overlay: {
            zIndex: 1000, // Ensure this value is higher than the z-index of SunEditor
            backgroundColor: "rgba(0, 0, 0, 0.6)",
          },
        }}
      >
        <Gallery
        dimension="2:3"
             onImageSelect={(selectedImage) => {
              setImage(selectedImage);
              setIsImageModalOpen(false); // Close modal after selecting an image
              IsSavedInputs();
            }}
        />
      </Modal>
            
              {/* Rest of your component */}
              <div className="my-2 bg-gray-100 p-4 rounded-md">
                <div className="flex items-center justify-center space-x-4">
                  <h1 className="text-gray-800 text-lg font-poppins">
                    Upload Logo :{" "}
                  </h1>
                  <div
                    className="border-2 border-blue-500 rounded-md flex items-center justify-center p-2 cursor-pointer w-48"
                    onClick={() => setIsImageModalOpen(true)}
                  >
                    {image ? (
                      <img
                        src={image}
                        alt="data"
                        className="w-full h-full object-cover rounded-md"
                      />
                    ) : (
                      <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-md">
                        <FiUpload size="1.5em" />
                      </button>
                    )}
                  </div>
                  {image && (
                    <div
                      className="text-red-500 cursor-pointer ml-4"
                      onClick={() => {
                        setImage(null);
                        IsSavedInputs();
                      }}
                    >
                      <AiFillDelete size="1.5em" />
                    </div>
                  )}
                  <p className="text-red-500 font-poppins text-sm mt-2">
                    {errors.image}
                  </p>{" "}
                  {/* Display image error message */}
                </div>
              </div>
            </div>
            <h1 className="text-gray-700 text-lg font-poppins p-4">
              Choose Header Type{" "}
            </h1>
            {/* Header 1 Representation */}
            <label
              className={`cursor-pointer block p-4 ${
                selectedHeader === "header1"
                  ? "border-2 border-indigo-500 rounded"
                  : "border border-gray-300 rounded hover:shadow-lg transition duration-300"
              }`}
            >
              <input
                type="radio"
                className="hidden"
                name="header"
                value="header1"
                checked={selectedHeader === "header1"}
                onChange={() => {
                  setSelectedHeader("header1");
                  IsSavedInputs();
                }}
              />
              <div className="flex justify-between items-center space-x-6">
                <div className="flex-shrink-0">
                  <img
                    src="https://www.jpencil.com/img/logo.png"
                    alt="logo"
                    className="w-48"
                  />
                </div>
                <div className="flex-grow bg-gray-200 flex items-center rounded-md p-2 shadow-md">
                  <BiSearch className={`${commonIconClasses} mr-2`} />
                  <input
                    type="text"
                    placeholder="Search..."
                    className={`${commonInputClasses} py-1 px-2 w-full rounded-lg`}
                  />
                </div>
                <div className="flex-shrink-0 flex space-x-4">
                  <FaRegHeart className={commonIconClasses} />
                  <FaShoppingCart className={commonIconClasses} />
                  <FaUser className={commonIconClasses} />
                </div>
              </div>
            </label>
            {/* Header 2 Representation */}
            <label
              className={`cursor-pointer block mt-3 p-4 ${
                selectedHeader === "header2"
                  ? "border-2 border-indigo-500 rounded"
                  : "border border-gray-300 rounded hover:shadow-lg transition duration-300"
              }`}
            >
              <input
                type="radio"
                className="hidden"
                name="header"
                value="header2"
                checked={selectedHeader === "header2"}
                onChange={() => {
                  setSelectedHeader("header2");
                  IsSavedInputs();
                }}
              />
              <div className="flex justify-between items-center">
                <div className="flex space-x-4">
                  <FaRegHeart className={commonIconClasses} />
                  <FaShoppingCart className={commonIconClasses} />
                  <FaUser className={commonIconClasses} />
                </div>
                <div className="mx-6 ">
                  <img
                    src="https://www.jpencil.com/img/logo.png"
                    alt="logo"
                    className="w-48"
                  />
                </div>
                <div className="bg-gray-200 flex items-center rounded-md p-2 shadow-md">
                  <BiSearch className={`${commonIconClasses} mr-2`} />
                  <input
                    type="text"
                    placeholder="Search..."
                    className={`${commonInputClasses} py-1 px-2 rounded-lg`}
                  />
                </div>
              </div>
            </label>
            <button
              onClick={handleSubmit}
              className={`bg-gradient-to-r from-indigo-500 to-blue-600 mt-4 hover:from-indigo-500 hover:to-blue-700 transition duration-300 ease-in-out text-white font-bold py-2 px-4 rounded-md block mx-auto ${
                Object.keys(errors).length > 0
                  ? "opacity-50 cursor-not-allowed"
                  : ""
              }`}
              disabled={Object.keys(errors).length > 0}
            >
              {Object.keys(errors).length > 0
                ? "Fill the required fields"
                : dataSubmitted
                ? "Update"
                : "Save"}
            </button>
          </div>
        </AccordionDetails>
      </Accordion>
      {/* header accordian ends */}
    </div>
  );
};

export default HeaderCustomization;
