import React, { useState } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import useTokenFromURL from "../../Context/useTokenFromURL";
import useAxios from "../../Axios/useAxios";
import { BASE_URL } from '../../config';
// Fetch the base URL from the environment variables.
const REACT_APP_BASE_URL = BASE_URL;

/**
 * ThemeCard Component:
 * Renders a clickable card to showcase individual themes with an image and name.
 * The card has visual feedback for selected and hover states.
 *
 * @param {string} themeName - The name of the theme.
 * @param {string} themeImage - The URL of the theme's image.
 * @param {boolean} isSelected - Indicates if the theme is currently selected.
 * @param {Function} onClick - Callback function for when the card is clicked.
 */
const ThemeCard = ({ themeName, themeImage, isSelected, onClick }) => {
  return (
    <div
      className={`w-1/4 h-80 m-4 border-2 rounded-lg cursor-pointer transition-all duration-300 shadow-md transform hover:scale-105 hover:rotate-3 ${
        isSelected ? "border-blue-500" : "border-gray-300"
      }`}
      onClick={onClick}
    >
      <div
        className="w-full h-full rounded-t-lg bg-cover bg-center flex items-end justify-center"
        style={{ backgroundImage: `url(${themeImage})` }}
      >
        {/* <img
          src={themeImage}
          alt={themeName}
          className="max-h-full max-w-full object-cover p-4"
        /> */}
      </div>
      <div className="w-full h-1/3 p-4 text-center">
        <p className="font-semibold text-lg">{themeName}</p>
      </div>
    </div>
  );
};

/**
 * ThemeSelector Component:
 * Provides a UI for users to select a theme. It integrates theme cards, displays a list
 * of themes to choose from, and provides feedback on theme selection.
 *
 * @param {Function} onThemeSelected - Callback function for when a theme is saved.
 */

const ThemeSelector = ({ onThemeSelected }) => {
  // Hooks to manage the authentication token and setup Axios configurations.
  useTokenFromURL();
  useAxios();

  // State to keep track of the selected theme.
  const [selectedTheme, setSelectedTheme] = useState(null);
  // Sample themes data..
  const themes = [
    {
      name: "Antique",
      image:
        "https://dashboard2.jpencil.com/image/client_3/thumbnail_image/1645768838735.jpg",
    },
    {
      name: "Contemporary",
      image:
        "https://media.tarangarts.com/cus_gallery/9b92d8e0-f98e-4836-bee0-c76ef4b9691c",
    },
    {
      name: "Modern",
      image:
        "https://www.darjewellery.com/product_image/s1200__aHR0cHM6Ly9tZWRpYS5kYXJqZXdlbGxlcnkuaW4vcHJvZHVjdF9pbWFnZXMvczEyMDBfXzE1OTkxMzI1NTk3MjcuanBn/22-kt-gold-half-varikai-haram.jpg",
    },
  ];

  /**
   * Function to save the selected theme.
   * Posts the selected theme to an endpoint and provides feedback to the user.
   */
  const saveTheme = async () => {
    // Check if a theme has been selected before proceeding.
    if (selectedTheme === null) {
      Swal.fire({
        icon: "warning",
        title: "Oops...",
        text: "Please select a theme before saving.",
        timer: 2000,
      });
      return;
    }
  
    try {
      // Make a POST request to save the selected theme.
      const response = await axios.post(
        `${REACT_APP_BASE_URL}/theme-manager`,
        {
          // Pass the selectedTheme index + 1 as theme_master_id
          theme_master_id: selectedTheme + 1, // This will pass 1 for the first theme, 2 for the second, and so on.
        }
      );
      // Provide feedback based on the response.
      if (response.status === 201) {
        onThemeSelected(); // Notify parent that a theme has been selected
        Swal.fire({
          icon: "success",
          title: "Success",
          text: "Theme saved successfully!",
          timer: 2000,
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Failed to save theme.",
          timer: 2000,
        });
      }
    } catch (error) {
      console.error(error);
      // Handle specific error scenario.
      if (error.response && error.response.data.message === "User_id already exist") {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Already selected the theme. Use Update Option",
          timer: 2000,
        });
      }
    }
  };
  

  return (
    <div className="min-h-screen bg-gray-100">
      <div className="container mx-auto py-16">
        <h1 className="text-center text-4xl mb-8">Select a theme</h1>
        <div className="flex justify-center items-start">
          {themes.map((theme, index) => (
            <ThemeCard
              key={index}
              themeName={theme.name}
              themeImage={theme.image}
              isSelected={selectedTheme === index}
              onClick={() => setSelectedTheme(index)}
            />
          ))}
        </div>
        <div className="flex justify-center mt-14">
          <button
            className="px-6 py-3 mr-3 text-white text-lg rounded-md transform hover:scale-105 transition-transform duration-200 ease-in-out shadow-lg hover:shadow-2xl bg-gradient-to-r from-blue-500 to-blue-700"
            onClick={saveTheme}
          >
            Save Theme
          </button>
          {/* {selectedTheme !== null && (
            <button
              className="px-6 py-3 text-white text-lg rounded-md transform hover:scale-105 transition-transform duration-200 ease-in-out shadow-lg hover:shadow-2xl bg-gradient-to-r from-green-500 to-green-700"
              onClick={updateTheme}
            >
              Update Theme
            </button>
          )} */}
        </div>
      </div>
    </div>
  );
};

export default ThemeSelector;
