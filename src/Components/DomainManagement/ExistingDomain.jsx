import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { BASE_URL } from '../../config';
import { FaRedo, FaCheckCircle, FaExclamationCircle, FaRegClock, FaCloudUploadAlt } from 'react-icons/fa';

const ExistingDomain = () => {
  const [domainUrl, setDomainUrl] = useState('');
  const [domainData, setDomainData] = useState(null);
  const [domainStatus, setDomainStatus] = useState(null);
  const [lastUpdated, setLastUpdated] = useState('');

  const fetchDomainData = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/domain/domain_status`);
      setDomainData(response.data);
      setDomainStatus(response.data.domain_status);
    } catch (error) {
      console.error('Error fetching domain data:', error);
    }
  };

  const handleRefreshClick = async () => {
    await fetchDomainData(); // Call the function to fetch domain data

    // Update and store the "Last Updated" time only if the domain status is "pending"
    if (domainStatus === 'pending') {
      const currentTime = new Date().toLocaleTimeString('en-IN', {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: true,
        timeZone: 'Asia/Kolkata'
      });
      const currentDate = new Date().toLocaleDateString('en-IN', { timeZone: 'Asia/Kolkata' });
      const currentDateTime = `${currentDate} ${currentTime}`;

      setLastUpdated(currentDateTime);
      localStorage.setItem('lastUpdated', currentDateTime);
    }
  };

  useEffect(() => {
    fetchDomainData();
    if (domainStatus === 'pending') {
      const storedTime = localStorage.getItem('lastUpdated');
      if (storedTime) {
        setLastUpdated(storedTime);
      }
    }
  }, [domainStatus]); // Add domainStatus as a dependency to re-run the effect when it changes

  const domainMessage = (status) => {
    switch (status) {
      case 'pending':
        return <span><FaExclamationCircle className="inline mr-2" />Your Name Server propagation is in progress
        it Generally take up to 24-48 hours for the change to take effect.</span>;
      case 'active':
        return <span><FaCheckCircle className="inline mr-2" />Your domain is ready! Go and customize your website builder.</span>;
      default:
        return '';
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await axios.post(`${BASE_URL}/domain/add_exist_domain`, { domain_name: domainUrl });
      fetchDomainData();
    } catch (error) {
      console.error('There was an error!', error);
    }
  };

  return (
    <div className="p-4 lg:p-8 bg-white shadow-lg rounded-lg font-poppins">
       <div className="flex justify-between items-center mb-4">
        <span className="text-lg lg:text-xl font-semibold text-indigo-800 flex items-center">
          {domainMessage(domainStatus)}
        </span>
        {domainStatus === 'pending' && (
          <button
            onClick={handleRefreshClick}
            className="flex items-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded transition duration-200 ease-in-out transform hover:-translate-y-1"
          >
            <FaRedo className="mr-2" /> Refresh
          </button>
        )}
      </div>
      {domainStatus === 'pending' && lastUpdated && (
        <p className="text-md text-gray-600 mb-4"><FaRegClock className="inline mr-2" />Last Updated: {lastUpdated}</p>
      )}


      <div className="flex flex-col lg:flex-row items-center justify-between gap-8">
        <div className="lg:w-1/2 flex justify-center">
          <img
            src="https://img.freepik.com/premium-vector/modern-design-illustration-domains-name_362714-1026.jpg"
            alt="Domain Setup"
            className="rounded-lg max-w-full h-auto"
          />
        </div>
        <div className="lg:w-1/2 space-y-4">
          <h3 className="text-2xl font-semibold text-indigo-600 flex items-center"><FaCloudUploadAlt className="mr-2" />Existing Domain Setup</h3>
          {domainData ? (
            <div className="space-y-2">
              <p className="text-lg font-semibold">Domain: {domainData.domain_name}</p>
              {domainData.renew_domain_date && (
                <p className="text-sm">Renew Domain Date: {domainData.renew_domain_date}</p>
              )}
              {domainData.renew_ssl_date && (
                <p className="text-sm">Renew SSL Date: {domainData.renew_ssl_date}</p>
              )}

              <h4 className="text-lg font-semibold flex items-center"><FaCheckCircle className="mr-2" />Name Servers:</h4>
              <div className="bg-gray-100 p-4 rounded-lg">
                <ul className="list-disc pl-5">
                  {domainData.name_servers.map((server, index) => (
                    <li key={index} className="text-indigo-600">{server}</li>
                  ))}
                </ul>
              </div>
            </div>
          ) : (
            <form onSubmit={handleSubmit} className="mt-4">
              <div className="flex items-center border-b border-indigo-500 py-2">
                <input
                  className="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                  type="text"
                  placeholder="Enter your domain URL"
                  aria-label="Domain URL"
                  value={domainUrl}
                  onChange={(e) => setDomainUrl(e.target.value)}
                />
                <button
                  className="flex items-center bg-indigo-500 hover:bg-indigo-700 text-white text-sm py-2 px-4 rounded transition duration-200 ease-in-out transform hover:scale-105"
                  type="submit"
                >
                  <FaCloudUploadAlt className="mr-2" />Submit
                </button>
              </div>
            </form>
          )}
        </div>
      </div>
    </div>
  );
};

export default ExistingDomain;
