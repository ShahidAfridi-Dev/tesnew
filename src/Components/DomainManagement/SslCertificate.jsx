import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { BASE_URL } from '../../config';
import { IoIosArrowRoundBack } from 'react-icons/io';
import { FaGlobeAmericas, FaTag,FaCircle, FaMoneyBillWave, FaPercent, FaCheckCircle, FaShoppingCart,FaCheck } from 'react-icons/fa';
import PaymentSuccess from './PaymentSuccess';
import { useToken } from '../../Context/TokenContext';
const SSLComponent = ({ onBack, domainName }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalContent, setModalContent] = useState('');
  const [sslOptions, setSslOptions] = useState([]);
  const [selectedOption, setSelectedOption] = useState(null);
  const [isPurchaseSuccessful, setIsPurchaseSuccessful] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const { jboId,token } = useToken();
  useEffect(() => {
    fetchSslRates();
  }, [domainName]);

  const fetchSslRates = async (sslType = '') => {
    setIsLoading(true);
    try {
      const url = `${BASE_URL}/domain/domainssl_rate?domain_name=${domainName}${sslType ? `&ssl_type=${sslType}` : ''}`;
      const response = await axios.get(url);
      if (sslType) {
        setSelectedOption({
          domain: response.data.domain,
          price: response.data.price,
          sslData: response.data.sslData[0], // Assuming the response will only contain one SSL data object when ssl_type is provided
          total: response.data.total,
          gst: response.data.gst,
        });
      } else {
        setSslOptions(response.data.sslData);
      }
      setIsLoading(false);
    } catch (error) {
      console.error('Error fetching SSL data:', error);
      setIsLoading(false);
    }
  };

  const handleSelection = (option) => {
    fetchSslRates(option.ssl_type);
  };

  const handlePurchase = async () => {
    // Payload data
    const innerPayload = {
      jbo_id: jboId.toString(), // Convert to string
      amount: selectedOption.total.toString(), // Convert to string and ensure it matches the desired format
    }
  
    // Stringify the inner payload
    const innerPayloadString = JSON.stringify(innerPayload)
    
    // Constructing the outer data object with innerPayloadString
    const data = { data: innerPayloadString }
    
    try {
      const response = await axios.post('https://j3sitebuilder.ta4.in/api/jbo_subscription/domainPurchase', data)
      // Assuming response.data.formbody contains the HTML content you want to display in the modal
      setModalContent(response.data.formbody)
      setIsModalVisible(true) // Show the modal
    } catch (error) {
      console.error('Error making API call:', error)
    }
  }
  
  
  


  if (isPurchaseSuccessful) {
    return <PaymentSuccess domainName={domainName} />;
  }

  return (
    <div className="p-8 max-w-4xl mx-auto bg-white rounded-xl shadow-xl transform transition duration-500 font-poppins">
      <div className="mb-8">
        <button onClick={onBack} className="flex items-center justify-center bg-indigo-600 hover:bg-indigo-700 transition-colors duration-200 text-white px-4 py-2 rounded-full shadow-md text-sm focus:outline-none">
          <IoIosArrowRoundBack className="text-lg" /> Back
        </button>
        <h3 className="text-xl font-semibold text-gray-800 mt-4">SSL Options for <span className="text-indigo-600">{domainName}</span> by JPencil</h3>
      </div>

      <p className="text-gray-700 mb-6 text-justify">
        In today digital marketplace, protecting your online store is essential. JPencil, understanding the critical need for security, offers tailored SSL solutions to protect your eCommerce platform and build customer trust. Explore the SSL options available to enhance your site's security:
      </p>

      {isLoading ? (
        <div className="grid md:grid-cols-2 gap-4 animate-pulse">
          {[1, 2].map((n) => (
            <div key={n} className="bg-gray-50 rounded-lg p-4">
              <div className="h-6 bg-gray-200 rounded mb-4"></div>
              <div className="h-4 bg-gray-200 rounded mb-2"></div>
              <div className="h-4 bg-gray-200 rounded"></div>
            </div>
          ))}
        </div>
      ) : (
        <div className="grid md:grid-cols-2 gap-4">
        {sslOptions.map((option) => (
          <div
            key={option.ssl_master_id}
            className={`bg-white rounded-lg p-4 shadow transition duration-300 hover:shadow-md cursor-pointer ${selectedOption?.sslData?.ssl_master_id === option.ssl_master_id ? 'border-2 border-indigo-600' : ''}`} // Conditional class for selected option
            onClick={() => handleSelection(option)} // Set selected option on click
          >
            <div className="flex justify-between items-center mb-3">
              <div className="text-lg font-medium text-gray-800">
                {option.ssl_type}
              </div>
              <div className={`text-sm font-semibold ${selectedOption?.sslData?.ssl_master_id === option.ssl_master_id ? 'text-indigo-600 flex items-center' : 'text-gray-500'}`}>
  {selectedOption?.sslData?.ssl_master_id === option.ssl_master_id ? <FaCheck style={{ color: 'green' }}/> :   <FaCircle style={{ color: 'blue' }}/>}
</div>
            </div>
            <p className="text-gray-600 text-justify">
              {option.ssl_master_id === 1 ? 'The Standard SSL is perfect for small to medium-sized eCommerce businesses looking to secure their online transactions and customer information.' : ''}
              {option.ssl_master_id === 2 ? 'Our Advanced SSL provides the highest level of security for large eCommerce platforms, ensuring the utmost protection for extensive online transactions and sensitive data.' : ''}
            </p>
            <div className="mt-4 text-indigo-600 font-semibold text-lg">
              ₹ {option.ssl_price}
            </div>
          </div>
        ))}
      </div>
      

      
      )}
      {selectedOption && (
        <div className="mt-8 p-6 bg-blue-50 rounded-lg shadow-lg relative">
          <h4 className="flex items-center text-xl font-semibold text-indigo-700 mb-4">
            <FaShoppingCart className="mr-2 text-indigo-500" />Checkout
          </h4>
          <div className="grid grid-cols-2 gap-4 text-gray-700 mb-6">
            <p className="flex items-center">
              <FaGlobeAmericas className="mr-2 text-indigo-500" />Domain: <span className="font-semibold ml-2">{selectedOption.domain}</span>
            </p>
            <p className="flex items-center">
              <FaCheckCircle className="mr-2 text-indigo-500" />SSL Type: <span className="font-semibold ml-2">{selectedOption.sslData.ssl_type}</span>
            </p>
            <p className="flex items-center">
              <FaTag className="mr-2 text-indigo-500" />Price: <span className="font-semibold ml-2">₹{selectedOption.sslData.ssl_price}</span>
            </p>
            <p className="flex items-center">
              <FaPercent className="mr-2 text-indigo-500" />GST: <span className="font-semibold ml-2">{selectedOption.gst}</span>
            </p>
            <p className="flex items-center">
              <FaMoneyBillWave className="mr-2 text-indigo-500" />Sub Total: <span className="font-semibold ml-2">₹{selectedOption.price}</span>
            </p>
          
            <p className="flex items-center">
              <FaMoneyBillWave className="mr-2 text-indigo-500" />Total Amount: <span className="font-semibold ml-2">₹{selectedOption.total}</span>
            </p>
          </div>
          <div className="flex justify-end">
            <button onClick={handlePurchase} className="bg-indigo-600 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline transform transition-colors duration-200">
              Purchase
            </button>
          </div>
        </div>
      )}
        {isModalVisible && (
        <div className="modal">
          <div className="modal-content" dangerouslySetInnerHTML={{ __html: modalContent }} />
          <button onClick={() => setIsModalVisible(false)}>Close</button>
        </div>
      )}
    </div>
  );
};

export default SSLComponent;
    