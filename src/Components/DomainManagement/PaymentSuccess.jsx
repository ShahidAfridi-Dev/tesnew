import React from 'react';

const PaymentSuccess = ({ domainName }) => {
  // Function to handle the invoice download
  const handleDownloadInvoice = () => {
    // Example: Navigate to a URL to download the invoice
    // This URL would be where your backend serves the invoice PDF
    // For demonstration, this is just a placeholder
    // const invoiceUrl = 'http://example.com/invoice.pdf';
    // window.open(invoiceUrl, '_blank');
  };

  return (
    <div className="max-w-4xl mx-auto p-6 bg-white rounded-lg shadow-xl">
      <div className="text-center">
        <img
          src="https://cdn.dribbble.com/users/614270/screenshots/14575431/media/4907a0869e9ed2ac4e2d1c2beaf9f012.gif"
          alt="Payment Success"
          className="mx-auto w-full h-auto"
          style={{ maxHeight: '480px' }}
        />
        <h2 className="text-2xl font-semibold text-indigo-700 mb-4">Payment Successful!</h2>
        <p className="text-gray-700">
          Your transaction for the domain <span className="font-semibold">{domainName}</span> has been processed successfully.
        </p>
        <p className="text-gray-600 mt-2 mb-4">
          Please wait for 24-48 hours for your domain to be set up. We appreciate your patience.
        </p>
        <button
          onClick={handleDownloadInvoice}
          className="bg-indigo-600 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline transform transition-colors duration-200"
        >
          Download Invoice
        </button>
      </div>
    </div>
  );
};

export default PaymentSuccess;
