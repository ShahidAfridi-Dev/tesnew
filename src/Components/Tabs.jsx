import React, { useState, useEffect, Children } from "react";
import GoDaddyUI from "./DomainManagement/GoDaddy";
import ExistingDomain from "./DomainManagement/ExistingDomain";
import WebsiteBuildeSteps from "./WebsiteBuilderSteps";
import "./style.css";
import ThemeSelector from "./ThemeManagement/ThemeManagement";
import WebsiteCustomization from "./WebsiteCustomization/WebsiteCustomization";
import HomePageCustomization from "./HomePageCustomization/HomePageCustomization";
import PagesCustomization from "./PagesCustomization/PagesCustomization";
import axios from "axios";
import useTokenFromURL from "../Context/useTokenFromURL";
import useAxios from "../Axios/useAxios";
import { useToken } from "../Context/TokenContext";
import Swal from "sweetalert2";
// Import BASE_URL from config.js
import { BASE_URL } from '../config';


/**
 * MultiStepForm Component:
 * This component manages a multi-step form which includes sections for
 * website building, domain management, customization and more.
 */
const MultiStepForm = () => {
  // Hooks to manage the authentication token and setup Axios configurations.
  useTokenFromURL();
  useAxios();
  // State declarations.
  const [step, setStep] = useState(1); // Current step in the multi-step form.
  const [userName, setUserName] = useState("ShahidAfridi"); // Name of the user.
  const [themeSelected, setThemeSelected] = useState(false); // Whether a theme has been selected or not.
  const [domainStatus,setDomainStatus]=useState("");
  const [loading, setLoading] = useState(false); // Loading state for async operations.
  const [tokenError, setTokenError] = useState(""); // Error related to token validation.
  const { token } = useToken();
  const { setToken } = useToken();
  // New state for domain selection
  const [domainSelection, setDomainSelection] = useState('existing'); // 'existing' or 'new'
  // const {token} = useToken();
  console.log("token Value", token);
  const { setJboId } = useToken();
  const [unsavedChanges, setUnsavedChanges] = useState(false); // Flag to check if there are any unsaved changes.
  const [hasInteracted, setHasInteracted] = useState(false); // Flag to check if the user has interacted with the form.

  /**
   * Method to validate the provided token.
   */
  // const TokenValid = async () => {
  //   try {
  //     const response = await axios.get(`${BASE_URL}`);
  //     console.log("responseData", response.data);
  //   } catch (error) {
  //     const errorData =
  //       error?.response?.data?.message !== null ? "Token is Invalid" : "";
  //     setTokenError(errorData);
  //     // console.error(error.response.data.message !== null, "errorData");
  //   }
  // };
  // // Effect hook to validate token on component mount.
  // useEffect(() => {
  //   TokenValid();
  // });
  /**
   * Method to fetch theme data and set the relevant state.
   */
  const fetchThemeManagerData = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/theme-manager`);
      setJboId(response.data[0].jbo_id);
      setLoading(true);
      response.data[0]?.selected_theme !== 4
        ? setThemeSelected(true)
        : setThemeSelected(false);
    } catch (error) {
      console.error(error);
      setThemeSelected(false);
    }
  };
  const fetchDomainData = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/domain/domain_status`);
      // setDomainData(response.data);
      setDomainStatus(response.data.domain_status);
    } catch (error) {
      console.error('Error fetching domain data:', error);
    }
  };
  console.log("themeSelectedStats", themeSelected);
  useEffect(() => {
    fetchThemeManagerData();
    fetchDomainData();
  });
  console.log("themeSelectedStatusnew", themeSelected);

  /**
   * Handler for tab change. It considers unsaved changes and user interactions
   * before deciding to change the tab.
   * @param {number} newStep - The step to navigate to.
   */
  const handleTabChange = (newStep) => {
    
    // Check if the "Reset Theme" tab is clicked and there are no unsaved changes or user hasn't interacted
    if (newStep === 6 && !unsavedChanges && !hasInteracted) {
      Swal.fire({
        title: 'Are you sure?',
        text: "Resetting the theme will erase all your current settings and data. Do you want to proceed",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, reset it!'
      }).then((result) => {
        if (result.isConfirmed) {
          resetTheme();
        }
      });
    } else if (!unsavedChanges && hasInteracted) {
      // Check for unsaved changes and user interaction for other tabs
      Swal.fire({
        title: "Unsaved changes",
        text: "You have unsaved changes. Are you sure you want to leave?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, leave",
        cancelButtonText: "No, stay",
      }).then((result) => {
        if (result.isConfirmed) {
          setUnsavedChanges(false); // Reset the unsaved changes flag
          setHasInteracted(false); // Reset the user interaction flag
          proceedTabChange(newStep); // Proceed to change the tab
        }
      });
    } else {
      // No unsaved changes or user interaction, proceed to change the tab
      proceedTabChange(newStep);
    }
  };
  
  const resetTheme = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/theme-manager/reset_theme`);
      if (response.status === 200) {
        setThemeSelected(false); // Reset the theme selection state
        setStep(3); // Switch to the ThemeSelector component
        Swal.fire('Reset!', 'Theme has been reset successfully.', 'success');
      }
    } catch (error) {
      console.error('Error resetting theme:', error);
      Swal.fire('Error!', 'There was a problem resetting the theme.', 'error');
    }
  };
  
  const proceedTabChange = (newStep) => {
    if (!themeSelected && newStep > 2) {
      setStep(3);
    } else {
      setStep(newStep);
    }
  };

  /**
   * Method to update the state when a theme is selected.
   */
  const onThemeSelected = () => {
    setThemeSelected(true);
  };
  // Tab labels for the multi-step form.
  const tabs = [
    "Website Builder Steps",
    "Domain Management",
    "HomePage Customization",
    "Pages Customization",
    "Website Customization",
    "Reset theme",
    "Website Live",
  ];
  return (
    <div className="bg-gradient-to-r from-purple-400 via-pink-500 to-red-500 min-h-screen font-poppins">
      {tokenError ? (
        <div className="flex items-center justify-center h-screen">
          <div className="w-[50rem]  shadow-lg rounded-lg p-5">
            <div className="flex justify-center items-center r">
              <img
                className="rounded-lg"
                src="https://blog.scorechain.com/wp-content/uploads/2022/10/Untitled-design-14.jpg"
                alt=""
              />
            </div>
            <h2 className="text-2xl text-white font-poppins mt-4 text-center">
              Invalid Token
            </h2>
            <p className="text-white text-xl font-poppins mt-2 text-center">
              Your Token is Invalid . Please,try again or register .
            </p>
          </div>
        </div>
      ) : (
        <div className="container mx-auto px-4 py-8">
          <div className="text-center mb-6">
            <h1 className="text-2xl md:text-3xl lg:text-2xl text-white font-bold mb-2 animate-pulse">
              Delighted to have you on board Mr {userName}..! Let's create
              something amazing together.
            </h1>
          </div>
          <div className="bg-white  p-6 rounded-lg shadow-lg grid grid-cols-1 lg:grid-cols-5 gap-4 ">
            <div className="col-span-1">
              <div className="space-y-4 font-poppins sticky top-8 ">
                {tabs.map((tab, index) => (
                  <button
                    key={tab}
                    onClick={() => handleTabChange(index + 1)}
                    className={`${
                      step === index + 1
                        ? "bg-indigo-500 text-white border-indigo-500"
                        : "bg-white text-indigo-500 border-indigo-500"
                    } w-full px-4 text-sm py-2 rounded-lg shadow-md focus:outline-none transition-all duration-300 ease-in-out transform hover:scale-105 ${
                      step === index + 1 ? "scale-110" : ""
                    }`}
                  >
                    {tab}
                  </button>
                ))}
              </div>
            </div>
            <div className="col-span-4 transition-all duration-300 ease-in-out">
              <div className="overflow-auto h-screen">
                {step === 1 && <WebsiteBuildeSteps />}
                {step === 2 && (
        <div className="flex flex-col items-center justify-center mt-4">
          <div className="">
            <label className="inline-flex items-center">
              <input
                type="radio"
                className="w-5 h-5 text-blue-600 form-radio focus:ring-blue-500 border-gray-300"
                name="domainType"
                value="existing"
                checked={domainSelection === 'existing'}
                onChange={() => setDomainSelection('existing')}
              />
              <span className="ml-2 text-gray-700">Existing Domain</span>
            </label>
            <label className="inline-flex items-center ml-6">
              <input
                type="radio"
                className="w-5 h-5 text-pink-600 form-radio focus:ring-pink-500 border-gray-300"
                name="domainType"
                value="new"
                checked={domainSelection === 'new'}
                onChange={() => setDomainSelection('new')}
              />
              <span className="ml-2 text-gray-700">New Domain</span>
            </label>
          </div>
          <div className="w-full pt-10">
            {domainSelection === 'existing' ? <ExistingDomain /> : <GoDaddyUI />}
          </div>
        </div>
      )}
                {!themeSelected && (step === 3 || step === 4) && (
                  <ThemeSelector onThemeSelected={onThemeSelected} />
                )}
                {themeSelected && step === 3 && (
                  <>
                    <HomePageCustomization />
                  </>
                )}
                {themeSelected && step === 4 && (
                  <>
                    <PagesCustomization
                      unsavedChanges={unsavedChanges}
                      setUnsavedChanges={setUnsavedChanges}
                      hasInteracted={hasInteracted}
                      setHasInteracted={setHasInteracted}
                    />
                  </>
                )}
                {step === 5 && (
                  <>
                    <WebsiteCustomization
                      unsavedChanges={unsavedChanges}
                      setUnsavedChanges={setUnsavedChanges}
                      hasInteracted={hasInteracted}
                      setHasInteracted={setHasInteracted}
                    />
                  </>
                )}
                {step === 7 && <div>Website Live</div>}
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default MultiStepForm;
