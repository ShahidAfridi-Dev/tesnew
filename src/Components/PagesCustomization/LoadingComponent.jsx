import React from 'react'
// Loading Component for custom page when data is laoding
const LoadingComponent = () => {
  return (
    <>
    
    <div className="flex flex-col bg-white border-2 border-gray-200 m-3 shadow-lg rounded-lg items-center space-y-8 p-8">
      <div className="w-1/3 h-8 bg-gray-300 shimmer"></div>
      <div className="w-full max-w-5xl">
        <div className="w-full h-20 bg-gray-300 rounded shimmer"></div>
      </div>
      <div className="w-full max-w-5xl">
        <div className="w-full h-20 bg-gray-300 rounded shimmer"></div>
      </div>
      <div className="w-full max-w-5xl">
        <div className="w-full h-20 bg-gray-300 rounded shimmer"></div>
      </div>
      <div className="w-full max-w-5xl">
        <div className="w-full h-20 bg-gray-300 rounded shimmer"></div>
      </div>
      <div className="w-full max-w-5xl space-y-4">
        {/* <div className="w-2/3 h-6 bg-gray-300 shimmer"></div> */}
        <div className="w-full h-20 bg-gray-300 shimmer"></div>
      </div>
      <div className="w-full max-w-5xl flex items-center space-x-4">
        <div className="w-6 h-6 bg-gray-300 rounded shimmer"></div>
        <div className="w-1/4 h-6 bg-gray-300 shimmer"></div>
      </div>
      <div className="w-1/4 h-10 bg-gray-300 shimmer"></div>
</div>

    
    </>
  )
}

export default LoadingComponent