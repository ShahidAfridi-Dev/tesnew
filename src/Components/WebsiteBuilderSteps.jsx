import React, { useState } from 'react';

const WebsiteBuilderSteps = () => {
  const [activeStep, setActiveStep] = useState(1);

  const steps = [
    {
      stepNumber: 1,
      title: 'Register Your Domain',
    },
    {
      stepNumber: 2,
      title: 'Select Your Theme',
    },
    {
      stepNumber: 3,
      title: 'Home Page Customization',
    },
    {
      stepNumber: 4,
      title: 'Website Customization',
    },
    { 
      stepNumber: 5,
      title: 'Make Website Live',
    },
  ];

  const onProceed = (stepNumber) => {
    setActiveStep(stepNumber + 1);
  };

  return (
    <div className="w-full min-h-screen bg-gray-100 flex flex-col items-center px-4 sm:px-10">
      {steps.map((step, index) => (
        <div
          key={step.stepNumber}
          className={`card w-full transform transition-all duration-500 ease-in-out p-4 m-4 rounded-lg shadow-lg ${
            activeStep === step.stepNumber ? 'scale-105' : ''
          }`}
        >
          <div className="flex flex-col sm:flex-row items-center justify-between w-full">
            <div className="flex-1 text-xl font-bold text-blue-600 mb-2 sm:mb-0">
              Step {step.stepNumber}
            </div>
            <div className="flex-1 font-semibold text-lg mb-2 sm:mb-0 font-poppins bg-clip-text text-transparent bg-gradient-to-r from-blue-400 via-purple-500 to-pink-600 whitespace-nowrap">
  {step.title}
</div>
            <div className="flex-1"></div>
            <button
              onClick={() => onProceed(step.stepNumber)}
              className={`proceed-btn flex-1 py-2 px-4 bg-blue-500 text-white rounded-lg shadow-md transform transition-all duration-300 ease-in-out hover:bg-blue-600 hover:scale-105 ${
                activeStep === step.stepNumber ? 'animate-pulse' : ''
              }`}
            >
              Proceed
            </button>
          </div>
        </div>
      ))}
    </div>
  );
};

export default WebsiteBuilderSteps;