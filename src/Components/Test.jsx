import React,{useState} from 'react'
import { SketchPicker } from "react-color"; // Import the SketchPicker
const Test = () => {
    const [color,setColor]=useState("");
    const handleTempColorChange = (color) => {
        setColor(color.hex);
    };
    console.log("colorData",color)
  return (
    <div>

<SketchPicker
          color={color}
          onChangeComplete={handleTempColorChange}
        />

    </div>
  )
}

export default Test