// Shimmer.js
import React from 'react';

const Shimmer = () => {
  return (
    <div className="animate-pulse">
      <div className="bg-gray-300 h-20 w-full mb-4 rounded-lg"></div>
      <div className="bg-gray-300 h-20 w-full mb-4 rounded-lg"></div>
      <div className="bg-gray-300 h-20 w-full mb-4 rounded-lg"></div>
      <div className="bg-gray-300 h-20 w-full mb-4 rounded-lg"></div>
    </div>
  );
};

export default Shimmer;