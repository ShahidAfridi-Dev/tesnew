import React, { useState, useEffect } from "react";
import { MdAdd, MdDelete } from "react-icons/md";
import { BsPencil } from "react-icons/bs";
import Swal from "sweetalert2";
import axios from "axios";
import ReactSwitch from "react-switch";
import BannerCustomization from "./Banner/BannerCustomization";
import CollageCustomization from "./Collage/CollageCustomization";
import InfoSliderCustomization from "./Information Slider/InformationSlider";
import SubscriptionCustomization from "./Subscription/Subscription";
import ProductSliderCustomization from "./ProductSlider/ProductSlider";
import CategorySliderCustomization from "./CategorySldier/CategorySlider";
import CmsBlockCustomization from "./CMSBlock/CmsBlock";
import useTokenFromURL from "../../Context/useTokenFromURL";
import useAxios from "../../Axios/useAxios";
// Import BASE_URL from config.js
import { BASE_URL } from '../../config';
const REACT_APP_BASE_URL = BASE_URL;
const DraggableWidget = ({
  title,
  updateFunc,
  deleteFunc,
  onDragOver,
  onDrop,
  onClick,
  onDragStart,
  draggable,
  widgetType,
  id,
  clickedWidgetId,
  isToggleVisible,
  toggleFuncs,
  activeWidget,
  widgetId,

  toggleBanner,
}) => {
  const isDraggable = widgetType !== "Banner" && draggable;
  const isActive = widgetId === clickedWidgetId;
  const isBanner = typeof title === "string" && widgetType === "Banner";
  const cssClass = isActive
    ? "flex justify-between items-center cursor-move border-2 border-indigo-500 p-4 m-2 rounded shadow-md transition duration-500 ease-in-out"
    : isBanner
    ? "flex justify-between cursor-move items-center border-2 border-gray-300 p-4 m-2 rounded shadow-md transition duration-500 ease-in-out"
    : "flex justify-between cursor-move items-center border-2 border-gray-300 p-4 m-2 rounded shadow-md transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110";
  const handleToggle = async (checked) => {
    console.log("checked Data", checked);
    // setToggleBanner(checked);
    try {
      await axios.patch(`${REACT_APP_BASE_URL}/custom-banner/${widgetId}`, {
        is_enabled: checked ? 1 : 0,
      });
     
      // call your original toggle function after successful API call
      toggleFuncs(checked);
    } catch (error) {
      console.error("Error updating banner:", error);
  
    }
  };
  return (
    <div
      draggable={isDraggable} // Update here
      onDragOver={onDragOver}
      onDrop={onDrop}
      onDragStart={onDragStart}
      id={id}
      onClick={onClick}
      className={`${cssClass} ${
        isBanner && toggleBanner ? "" : isBanner ? "opacity-50" : ""
      }`}
    >
      <h2 className="font-bold text-gray-700">{title}</h2>
      <div className="flex items-center">
        {isToggleVisible ? (
          <label>
            <ReactSwitch
              checked={toggleBanner}
              onChange={handleToggle}
              offColor="#767777"
              onColor="#81b0ff"
              offHandleColor="#ffffff"
              onHandleColor="#ffffff"
              handleDiameter={18}
              uncheckedIcon={false}
              checkedIcon={false}
              boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
              activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
              height={18}
              width={30}
              className="react-switch"
              // disabled={bannerDataSubmit}
            />
          </label>
        ) : (
          <>
            {/* <BsPencil
              onClick={updateFunc}
              className="text-indigo-500 hover:text-indigo-700 cursor-pointer mr-4"
              size={20}
            /> */}
            <MdDelete
              onClick={deleteFunc}
              className="text-red-500 hover:text-red-700 cursor-pointer"
              size={24}
            />
          </>
        )}
      </div>
    </div>
  );
};

const WidgetModal = ({ isVisible, addFunc, hideFunc }) => {
  const widgetTypes = [
    "Collage",
    "Info Slider",
    "Product Slider",
    "Category Slider",
    "Subscription",
    "Cms Custom"
  ];

  const [selectedWidgetType, setSelectedWidgetType] = useState(widgetTypes[0]);

  const handleClick = () => {
    addFunc(selectedWidgetType);
    setSelectedWidgetType(widgetTypes[0]);
    hideFunc();
  };

  return (
    isVisible && (
      <div className="fixed z-10 inset-0 overflow-y-auto">
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <div className="fixed inset-0 transition-opacity" aria-hidden="true">
            <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
          </div>
          <span
            className="hidden sm:inline-block sm:align-middle sm:h-screen"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
              <div className="sm:flex sm:items-start">
                <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                  <h3 className="text-lg leading-6 font-medium text-gray-900">
                    Add a widget
                  </h3>
                  <div className="mt-2">
                    <select
                      value={selectedWidgetType}
                      onChange={(e) => setSelectedWidgetType(e.target.value)}
                      className="w-full px-3 py-2 text-base text-gray-700 bg-white rounded-md focus:outline-none"
                    >
                      {widgetTypes.map((type, index) => (
                        <option value={type} key={index}>
                          {type}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
              <button
                type="button"
                onClick={handleClick}
                className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-600 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none sm:ml-3 sm:w-auto sm:text-sm"
              >
                Add
              </button>
              <button
                type="button"
                onClick={hideFunc}
                className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  );
};

const AddWidgetButton = ({ showFunc }) => {
  return (
    <div className="flex justify-center items-center m-2">
      <MdAdd
        onClick={showFunc}
        className="text-green-500 hover:text-green-700 cursor-pointer transform transition duration-500 hover:scale-125"
        size={24}
      />
    </div>
  );
};

const SectionOne = ({
  activeWidget,
  setActiveWidget,
  clickedWidgetId,
  setClickedWidgetId,
  bannerDataSubmit,
  fetchWidgetOrder,
  widgets,
  setWidgets,
  widgetCounts,
  setWidgetCounts,
  setWidgetName,
  toggleBanner,
  setToggleBanner,
  setHasInteracted,
  refresh,
  setRefresh,
  setUnsavedChanges,
  unsavedChanges,
  hasInteracted,
}) => {
  const [widgetOrders, setWidgetOrders] = useState([]);
  const [lastClickedWidgetType, setLastClickedWidgetType] = useState(null);  // to keep track of the last clicked widgetType
  const [isModalVisible, setIsModalVisible] = useState(false);
  const onDragStart = (e, index) => {
    if (widgets[index].widget_type !== "banner") {
      e.dataTransfer.setData("itemIndex", index);
    }
  };
  //   const onDrop = (e, index) => {
  //     const draggedIndex = e.dataTransfer.getData("itemIndex");
  //     const newList = Array.from(widgets);
  //     const [removed] = newList.splice(draggedIndex, 1);
  //     newList.splice(index, 0, removed);
  // console.log("newList",newList);
  //     setWidgets(newList);
  //   };
  const onDrop = (e, index) => {
    if (index !== 0) {
      const draggedIndex = e.dataTransfer.getData("itemIndex");
      const newList = Array.from(widgets);
      const [removed] = newList.splice(draggedIndex, 1);
      newList.splice(index, 0, removed);

      setWidgets(newList);

      // Prepare the payload for the PATCH request
      const updatedWidgets = newList.map((widget, idx) => ({
        widget_order_id: widget.widgetId,
        widget_name: widget.widget_name,
        order_by: idx + 1,
      }));

      // Make the PATCH request
      axios
        .patch(`${REACT_APP_BASE_URL}/widget-order`, updatedWidgets)
        .then((response) => {
          fetchWidgetOrder();
             // Show SweetAlert2 notification after the successful API call
    Swal.fire({
      title: 'Success!',
      text: 'Widget order has been updated.',
      icon: 'success',
      showConfirmButton: false,
        timer: 1500,
    });
    
        })
        .catch((err) => {
          console.error(err);
               // Optionally, show an error alert
    Swal.fire({
      title: 'Error!',
      text: 'Failed to update widget order.',
      icon: 'error',
      showConfirmButton: false,
        timer: 1500,
    });
        });
    }
  };

  // Event handlers...
  // Event handlers...
  const handleWidgetClick = (widget) => {
    console.log("unsavedChanges",unsavedChanges);
    console.log("hasInteracted",hasInteracted);
    if (unsavedChanges && hasInteracted) {
      Swal.fire({
        title: "Unsaved changes",
        text: "You have unsaved changes. Are you sure you want to leave?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, leave",
        cancelButtonText: "No, stay",
      }).then((result) => {
        if (result.isConfirmed) {
          setUnsavedChanges(false);
          setHasInteracted(false);
          setRefresh(null);
          setActiveWidget(widget.widgetType); // Set the new widget only if user confirmed
          setWidgetName(widget.widget_name);
          setClickedWidgetId(widget.widgetId);
          setUnsavedChanges(true);
        }
      });
    } else {
      setActiveWidget(widget.widgetType); // Set the new widget if there are no unsaved changes
      setWidgetName(widget.widget_name);
      setClickedWidgetId(widget.widgetId);
      setRefresh(!refresh);
    
    }
  };




  const fetchWidgetOrderAdd = (widgetName) => {
    console.log("widgetName", widgetName.widgetType);

    setWidgets([...widgets, widgetName]);

    // widgets.push({widgetType: widgetName.widgetType})
    fetchWidgetOrder();
  };

  useEffect(() => {
    fetchWidgetOrder();
  }, []);

  const onDragOver = (e) => {
    e.preventDefault();
  };
  const updateFunc = () => console.log("Update Function");

  const deleteFunc = async (index, widgetId) => {
    const title = widgets[index].widget_name;
    const type = title?.split(" ")[0];
    // const count = widgetCounts[type] - 1;

    const result = await Swal.fire({
      title: "Are you sure?",
      text: "You want to delete it!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    });

    if (result.isConfirmed) {
      axios
        .delete(`${REACT_APP_BASE_URL}/widget-order/` + widgetId)
        .then((response) => {
          const data = response.data;
          if (data.status === "success") {
            Swal.fire("Deleted!", "Your widget has been deleted.", "success");
            fetchWidgetOrder();
            // setWidgets(widgets.filter((_, i) => i !== index));
            // setWidgetCounts({ ...widgetCounts, [type]: count });
          } else {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: "Something went wrong!",
            });
          }
        })
        .catch((error) => {
          console.error("Error:", error);
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Something went wrong!",
          });
        });
    }
  };
  const createWidgetOrder = (title, order, type) => {
   

    // count how many widgets with this title exist
    const widgetCount = widgets.reduce(
      (count, widget) =>
        widget.widget_name.startsWith(title) ? count + 1 : count,
      0
    );
    // append the count to the title
    const newTitle = `${title} ${widgetCount + 1}`;

    const payload = {
      widget_name: newTitle,
      widget_type: type,

    };
    console.log("types", type);
    axios
      .post(`${REACT_APP_BASE_URL}/widget-order`, payload)
      .then((response) => {
        const order = widgets[widgets.length - 1].order_by;
        fetchWidgetOrderAdd({
          widget_name: newTitle,
          // widgetType: type, // change here: use original type, not from newTitle
          order_by: order,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  console.log("widgets", widgets);
  const addFunc = (type) => {
    const count = widgetCounts[type] || 0;
    const title = count > 1 ? `${type} ${count + 1}` : type;

    setWidgetCounts({ ...widgetCounts, [type]: count + 1 });

    const maxOrder = widgetOrders.length > 0 ? Math.max(...widgetOrders) : 0;
    const order = maxOrder + 1;

    setWidgetOrders([...widgetOrders, order]);

    createWidgetOrder(title, order, type); // Here, pass the newly formed 'title', order and type
  };

  return (
    <div className="w-full  md:w-1/2 lg:w-[60%] xl:w-[40%]  border-gray-300 p-4 overflow-y-auto h-screen">
      <h1 className="text-lg font-poppins text-gray-700 flex justify-center">
        Widget Order
      </h1>
      {widgets.map((widget, index) => (
        <DraggableWidget
          className="cursor-pointer"
          title={widget.widget_name}
          onClick={() => handleWidgetClick(widget)}
          updateFunc={updateFunc}
          deleteFunc={() => deleteFunc(index, widget.widgetId)}
          onDragOver={onDragOver}
          onDrop={(e) => onDrop(e, index)}
          onDragStart={(e) =>
            widget.widget_type !== "banner" && onDragStart(e, index)
          }
          id={`widget-${index}`}
          isToggleVisible={index === 0}
          toggleFuncs={() => setToggleBanner(!toggleBanner)} // Corrected prop name and function
          bannerDataSubmit={bannerDataSubmit}
          widgetType={widget.widgetType}
          widgetId={widget.widgetId}
          clickedWidgetId={clickedWidgetId}
          toggleBanner={toggleBanner}
          activeWidget={activeWidget}
          draggable={widget.widgetType !== "Banner"}
          key={index}
        />
      ))}
      <AddWidgetButton showFunc={() => setIsModalVisible(true)} />
      <WidgetModal
        isVisible={isModalVisible}
        addFunc={addFunc}
        hideFunc={() => setIsModalVisible(false)}
      />
    </div>
  );
};

const SectionTwo = ({
  activeWidget,
  clickedWidgetId,
  setToggleBanner,
  toggleBanner,
  refresh,
  setBannerDataSubmit,
  fetchWidgetOrder,
  widgetName,
  setActiveWidget,
  setHasInteracted,
  setUnsavedChanges,
  unsavedChanges,
  hasInteracted,
}) => {
  console.log("sectionTwoWidgeName", widgetName);

  const widgetType = activeWidget ? activeWidget.match(/^\D+/)[0].trim() : "";
  // useEffect(() => {
  //   if (unsavedChanges && hasInteracted) {
  //     Swal.fire({
  //       title: "Unsaved changes",
  //       text: "You have unsaved changes. Are you sure you want to leave?",
  //       icon: "warning",
  //       showCancelButton: true,
  //       confirmButtonText: "Yes, leave",
  //       cancelButtonText: "No, stay",
  //     }).then((result) => {
  //       if (result.isConfirmed) {
  //         setUnsavedChanges(false);
  //         setHasInteracted(false);
  //       } else {
  //         // Stay in the current component or handle the situation as per your requirement
  //       }
  //     });
  //   }
  // }, [activeWidget]);

  const renderContent = () => {
    if (!activeWidget) return null; // Add guard clause if activeWidget is not set

    switch (widgetType) {
      case "Banner":
        return toggleBanner ? (
          <BannerCustomization
            widgetType={widgetType}
            widgetName={widgetName}
            widgetId={clickedWidgetId}
            setToggleBanner={setToggleBanner}
            hasInteracted={hasInteracted}
            setHasInteracted={setHasInteracted}
            setBannerDataSubmit={setBannerDataSubmit}
            setUnsavedChanges={setUnsavedChanges}
            fetchWidgetOrder={fetchWidgetOrder}
          />
        ) : null;
      case "Collage":
        return (
          <CollageCustomization
            widgetType={widgetType}
            unsavedChanges={unsavedChanges}
            fetchWidgetOrder={fetchWidgetOrder}
            setUnsavedChanges={setUnsavedChanges}
            setHasInteracted={setHasInteracted}
            hasInteracted={hasInteracted}
            refresh={refresh}
            widgetId={clickedWidgetId}
          />
        );
      case "Info Slider":
        return(<InfoSliderCustomization widgetType={widgetType}
          unsavedChanges={unsavedChanges}
          fetchWidgetOrder={fetchWidgetOrder}
          setUnsavedChanges={setUnsavedChanges}
          setHasInteracted={setHasInteracted}
          hasInteracted={hasInteracted}
          refresh={refresh}
          widgetId={clickedWidgetId}/>) 
      case "Product Slider":
        return (<ProductSliderCustomization widgetType={widgetType}
          unsavedChanges={unsavedChanges}
          fetchWidgetOrder={fetchWidgetOrder}
          setUnsavedChanges={setUnsavedChanges}
          setHasInteracted={setHasInteracted}
          hasInteracted={hasInteracted}
          refresh={refresh}
          widgetId={clickedWidgetId}/>);
      case "Category Slider":
        return (<CategorySliderCustomization widgetType={widgetType}
          unsavedChanges={unsavedChanges}
          fetchWidgetOrder={fetchWidgetOrder}
          setUnsavedChanges={setUnsavedChanges}
          setHasInteracted={setHasInteracted}
          hasInteracted={hasInteracted}
          refresh={refresh}
          widgetId={clickedWidgetId}/>);
      case "Subscription":
        return (<SubscriptionCustomization widgetType={widgetType}
          unsavedChanges={unsavedChanges}
          fetchWidgetOrder={fetchWidgetOrder}
          setUnsavedChanges={setUnsavedChanges}
          setHasInteracted={setHasInteracted}
          hasInteracted={hasInteracted}
          refresh={refresh}
          widgetId={clickedWidgetId}/>);
      case "Cms Custom":
        return (<CmsBlockCustomization widgetType={widgetType}
          unsavedChanges={unsavedChanges}
          fetchWidgetOrder={fetchWidgetOrder}
          setUnsavedChanges={setUnsavedChanges}
          setHasInteracted={setHasInteracted}
          hasInteracted={hasInteracted}
          refresh={refresh}
          widgetId={clickedWidgetId}/>);
      default:
        return (
          <BannerCustomization
            widgetType={widgetType}
            widgetName={widgetName}
          />
        );
    }
  };

  return (
    <div className="w-full  md:w-1/2 lg:w-full xl:w-full  border-gray-300 p-4 overflow-y-auto h-screen">
      {renderContent()}
    </div>
  );
};

const HomePageCustomization = () => {
  useTokenFromURL();
  useAxios();
  const [activeWidget, setActiveWidget] = useState(null);
  const [widgetName, setWidgetName] = useState(null);
  const [clickedWidgetId, setClickedWidgetId] = useState(null);
  const [unsavedChanges, setUnsavedChanges] = useState(false);
  const [hasInteracted, setHasInteracted] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const [bannerDataSubmit, setBannerDataSubmit] = useState(false);
  const initialWidgetCounts = {
    "Banner": 1,
    " Collage": 1,
    "Info Slider": 1,
    "Product Slider": 1,
    "Category Slider": 1,
    "Subscription": 1,
    "Cms Custom": 1,
  };
  const [widgets, setWidgets] = useState([]);
  const [toggleBanner, setToggleBanner] = useState(false);
  const [widgetCounts, setWidgetCounts] = useState(initialWidgetCounts);
  const fetchWidgetOrder = () => {
  
    axios
      .get(`${REACT_APP_BASE_URL}/widget-order/`)
      .then((res) => {
        // Check data is in correct format
        if (res.data && Array.isArray(res.data)) {
          // Sort the data based on the 'order_by' field
          const sortedData = res.data.sort((a, b) => a.order_by - b.order_by);

          let widgetArray = [];
          let widgetTypesList = [];
          const counts = {};
          sortedData.forEach((item) => {
            const type = item.widget_name; // 'Banner', 'Collage', etc.
            if (!widgetTypesList?.includes(type) && type !== "Banner")
              widgetTypesList.push(type); // Exclude 'Banner'
            counts[type] = (counts[type] || 0) + 1;

            // Add widget object to array
            widgetArray.push({
              widget_name: item.widget_name,
              widgetType: item.widget_type,
              order_by: item.order_by,
              widgetId: item.widget_order_id,
            });
          });
          setToggleBanner(res.data[0].is_enabled === 1 ? true : false);
          setWidgets(widgetArray);
          setWidgetCounts(counts);
        }
      })
      .catch((err) => console.error(err));
  };
  return (
    <div className="flex h-screen flex-col sm:flex-row">
      <SectionOne
        activeWidget={activeWidget}
        setActiveWidget={setActiveWidget}
        clickedWidgetId={clickedWidgetId}
        setClickedWidgetId={setClickedWidgetId}
        bannerDataSubmit={bannerDataSubmit}
        fetchWidgetOrder={fetchWidgetOrder}
        unsavedChanges={unsavedChanges}
        setUnsavedChanges={setUnsavedChanges}
        hasInteracted={hasInteracted}
        setHasInteracted={setHasInteracted}
        refresh={refresh}
        setRefresh={setRefresh}
        widgets={widgets}
        setWidgets={setWidgets}
        widgetCounts={widgetCounts}
        setWidgetCounts={setWidgetCounts}
        setWidgetName={setWidgetName}
        setToggleBanner={setToggleBanner}
        toggleBanner={toggleBanner}
      />
      <SectionTwo
        activeWidget={activeWidget}
        setActiveWidget={setActiveWidget}
        toggleBanner={toggleBanner}
        unsavedChanges={unsavedChanges}
        setUnsavedChanges={setUnsavedChanges}
        hasInteracted={hasInteracted}
        refresh={refresh}
        setHasInteracted={setHasInteracted}
        setToggleBanner={setToggleBanner}
        clickedWidgetId={clickedWidgetId}
        setBannerDataSubmit={setBannerDataSubmit}
        fetchWidgetOrder={fetchWidgetOrder}
        widgetName={widgetName}
      />
    </div>
  );
};

export default HomePageCustomization;
