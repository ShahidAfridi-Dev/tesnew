import React from 'react'

const Skeleton = () => {
  return (
    <div>

<div className="p-4">

    {/* Title */}
    <div className="animate-pulse  bg-gray-400 rounded w-3/4 mx-auto"></div>

    {/* Form */}
    <div className="space-y-4 mt-4 animate-pulse">
        <div className="flex space-x-4">
            <div className="h-10 bg-gray-300 rounded w-1/2"></div>
            <div className="h-10 bg-gray-300 rounded w-1/2"></div>
        </div>
    </div>

    {/* Category Slider Visual */}
    <div className="mt-4 animate-pulse">
        <div className="h-6 bg-gray-400 rounded w-1/3"></div>
        <div className="flex space-x-4 mt-4">
            <div className="h-10 bg-gray-300 rounded w-2/3"></div>
            <div className="h-10 bg-gray-300 rounded w-1/3"></div>
        </div>
    </div>

    {/* Menu Details */}
    <div className="mt-4 animate-pulse">
        <div className="h-6 bg-gray-400 rounded w-1/3"></div>
        <div className="space-y-4 mt-4">
            <div className="flex space-x-4">
                <div className="h-10 bg-gray-300 rounded w-[70%]"></div>
                <div className="h-10 bg-gray-300 rounded w-1/3"></div>
            </div>
            <div className="h-10 bg-gray-300 rounded w-2/3 mt-4"></div>
            <div className="h-10 bg-gray-300 rounded w-2/3 mt-4"></div>
        </div>
    </div>

</div>


    </div>
  )
}

export default Skeleton