import React,{useState} from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import NestedSelect from "../NestedSelect";

import { AiOutlineDelete } from "react-icons/ai";
const ProductDetailAccordian = ({
  productSelection,
  selectedValue,
  setSelectedOption,
  setSelectedValue,
  removeSku,
  updateSku,
  skuError,
  isButtonDisabled,
  addSku,
  skuList,
  setProductSelection,
  selectedOption,
  productDetails,
  handleSelectValueChange,
  handleNumProductsChange,
  data
}) => {

  return (
    <>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Product Details</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="flex justify-start items-center space-x-6">
            <div>
              <label>
                <input
                  type="radio"
                  value="Automatic"
                  checked={productSelection === "Automatic"}
                  onChange={(e) => setProductSelection(e.target.value)}
                />
                <span className="ml-2 font-poppins">
                  Automatic Product Selection
                </span>
              </label>
            </div>
            <div>
              <label>
                <input
                  type="radio"
                  value="Custom"
                  checked={productSelection === "Custom"}
                  onChange={(e) => setProductSelection(e.target.value)}
                />
                <span className="ml-2 font-poppins">
                  Custom Product Selection
                </span>
              </label>
            </div>
          </div>
          {productSelection === "Automatic" && (
            <div className="w-2/3">
              <div className="flex flex-col space-y-4">
                {/* Select box */}
                <select
                  value={productDetails.view_selected}
                  onChange={handleSelectValueChange}
                  className="mt-4 font-poppins bg-white rounded-md border border-gray-300 focus:ring-indigo-500 focus:border-indigo-500 text-base"
                >
                  <option value="bestSellingGlobal">
                    Best Selling Products (Global)
                  </option>
                  <option value="topRated">Top-Rated Products</option>
                  <option value="bestSellingLocal">
                    Best Selling Products (Local)
                  </option>
                  <option value="latestArrivals">Latest Arrivals</option>
                </select>

                <div>
                  <Typography> Number of Products </Typography>

                  <input
                    id="number_of_product"
                    name="number_of_product"
                    type="number"
                    min={1}
                    max={20}
                    value={productDetails.number_of_product}
                    onChange={handleNumProductsChange}
                    className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                  />
                </div>
                {/* Menu filter input */}
                <div>
                  <Typography> Menu Filter </Typography>
                  <div className="relative w-full">
                    <NestedSelect
                      productDetails={productDetails}
                      selectedOption={selectedOption}
                      setSelectedOption={setSelectedOption}
                      selectedValue={selectedValue}
                      setSelectedValue={setSelectedValue}
                      data={data}
                    />
                  </div>
                </div>
              </div>
            </div>
          )}

          {productSelection === "Custom" && (
            <div className="py-5">
              {/* SKU Input and Delete Icons */}
              {skuList.map((sku) => (
  <div key={sku.id} className="flex items-start space-x-4 mb-3">
    <label className="text-sm font-medium text-gray-700 self-center">
      SKU
    </label>
    <div className="flex flex-col w-1/3">
      <input
        type="text"
        placeholder="Enter SKU"
        value={sku.sku}
        onChange={(e) => updateSku(sku.id, e.target.value)}
        className="focus:ring-indigo-500 focus:border-indigo-500 block sm:text-sm border-gray-300 rounded-md"
      />
      {/* Show error message specific to this SKU */}
      {skuError[sku.id] && <p className="text-red-500 mt-2">{skuError[sku.id]}</p>}
    </div>
    <div className="self-center">
      <AiOutlineDelete
        className="text-red-500 cursor-pointer"
        size={24}
        onClick={() => removeSku(sku.id)}
      />
    </div>
  </div>
))}



<button
  onClick={addSku}
  disabled={isButtonDisabled}
  className={`bg-indigo-500 text-white rounded px-4 py-1 mt-3 ${isButtonDisabled ? "opacity-50 cursor-not-allowed" : ""}`}
>
  Add SKU
</button>
            </div>
          )}
        </AccordionDetails>
      </Accordion>
    </>
  );
};

export default ProductDetailAccordian;
