import React from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ReactSwitch from "react-switch";
const ProductVisualAccordian = ({ productVisuals, handleVisualChange }) => {
  return (
    <>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Product Slider Visual</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="flex space-x-4">
            <div className="w-2/3">
              <label
                htmlFor="widgetNameInput"
                className="block text-sm font-medium text-gray-700"
              >
                Slider Header
              </label>
              <input
                id="sliderHeader"
                name="sliderHeader"
                type="text"
                placeholder={productVisuals.sliderHeader}
                value={productVisuals.sliderHeader}
                onChange={(e) =>
                  handleVisualChange("sliderHeader", e.target.value)
                }
                className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
              />
            </div>
          </div>
          <div className="flex space-x-4 mt-4">
            <div className="w-2/3">
              <label
                htmlFor="widgetNameInput"
                className="block text-sm font-medium text-gray-700"
              >
                Number of Products at a time (Max: 4)
              </label>
              <input
                id="rotationSeconds"
                name="rotationSeconds"
                type="number"
                min={1}
                max={4}
                value={productVisuals.numProducts}
                onChange={(e) =>
                  handleVisualChange("numProducts", e.target.value)
                }
                className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
              />
            </div>
          </div>
          <div className="flex space-x-4 mt-3">
            <div className="w-2/3">
              <div className="flex space-x-4 mt-3">
                <div className="w-full mt-4">
                  <Typography>Product Detail to be Displayed</Typography>

                  {/* Container for all the switches */}
                  <div className="flex mt-4 justify-between items-center">
                    {/* Product Name Switch */}
                    <div className="flex items-center">
                      <label className="mr-3 text-sm font-poppins">
                        Product Name
                      </label>
                      <ReactSwitch
                        handleDiameter={18}
                        checked={productVisuals.showProductName}
                        onChange={() =>
                          handleVisualChange(
                            "showProductName",
                            !productVisuals.showProductName
                          )
                        }
                      />
                    </div>

                    {/* Price Switch */}
                    <div className="flex items-center">
                      <label className="mr-3 text-sm font-poppins">Price</label>
                      <ReactSwitch
                        handleDiameter={18}
                        checked={productVisuals.showPrice}
                        onChange={() =>
                          handleVisualChange(
                            "showPrice",
                            !productVisuals.showPrice
                          )
                        }
                      />
                    </div>

                    {/* SKU Switch */}
                    <div className="flex items-center">
                      <label className="mr-3 text-sm font-poppins">SKU</label>
                      <ReactSwitch
                        handleDiameter={18}
                        checked={productVisuals.showSKU}
                        onChange={() =>
                          handleVisualChange("showSKU", !productVisuals.showSKU)
                        }
                      />
                    </div>
                  </div>
                  <div className="flex space-x-4 mt-8">
                    <div className="w-2/3">
                      <label
                        htmlFor="widgetNameInput"
                        className="block text-sm font-medium text-gray-700"
                      >
                        Rotation Seconds (0 for No rotation)
                      </label>
                      <input
                        id="rotationSeconds"
                        name="rotationSeconds"
                        type="number"
                        min={1}
                        max={12}
                        value={productVisuals.rotationSeconds}
                        onChange={(e) =>
                          handleVisualChange("rotationSeconds", e.target.value)
                        }
                        className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </AccordionDetails>
      </Accordion>
    </>
  );
};

export default ProductVisualAccordian;
