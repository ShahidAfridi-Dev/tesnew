import React, { useState, useEffect } from "react";
import useAxios from "../../../Axios/useAxios";
import useTokenFromURL from "../../../Context/useTokenFromURL";
import axios from "axios";
import Swal from "sweetalert2";
import SunEditor from "suneditor-react";
import "suneditor/dist/css/suneditor.min.css";
import Skeleton from "./Skeleton"
import { BASE_URL } from '../../../config';

// Base URL fetched from global config
const REACT_APP_BASE_URL = BASE_URL;

const CmsBlock = ({widgetType,unsavedChanges,fetchWidgetOrder,setUnsavedChanges,setHasInteracted,refresh,widgetId}) => {

       // Using hooks to handle authentication and setup for axios
  useTokenFromURL();
  useAxios();

  // Component state initialization
  const [formValues, setFormValues] = useState({
    widget_name: "",
    widgetTypeInput: "",
    
  });
  const [customId, setCustomId] = useState(null);
  const [loading,setLoading]=useState(false);
  const [DataSubmitted, setDataSubmitted] = useState(false);
  const [CustomContentData, setCustomContentData] = useState('<p><br></p>'); // For SunEditor content
  const [errors, setErrors] = useState([]);

      // Handlers for custom editor
  const handleEditorChange = (contentData) => {
    console.log("contentDtanew")
    setCustomContentData(contentData);
      IsSavedInputs();
  };
     // Handlers for unsaved Changes data
  const IsSavedInputs = () => {
    setHasInteracted(true);
    setUnsavedChanges(true);
};
   // Handlers for formValues
  const handleChange = (e) => {
    // setIsFormSubmitted(true);
    const { name, value } = e.target;
    setFormValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));
    IsSavedInputs();
  };
console.log("contentData",CustomContentData)
   // Fetching existing widget data using widgetId
  const fetchCustomData = async ()=>{
    try {
        const response = await axios.get(
          `${REACT_APP_BASE_URL}/cms-custom/${widgetId}`
        );
        setLoading(true);
        console.log("responseData",response.data[0])
        setFormFields(response.data[0]);
       
      } catch (error) {
        setLoading(true);
        console.error(error);
      }
}
 // Setting form fields from fetched data
const setFormFields =(data)=>{
    setFormValues({
        widget_name: data.widget_name,
        widgetTypeInput: data.widget_type,
       
      });
      setCustomContentData(data.cms_custom_editor)
      setCustomId(data.cms_custom_id);
      const formsubmit = data.cms_custom_editor !== null ? true : false;
      setDataSubmitted(formsubmit);
}
 // Fetch data when 'refresh' changes
useEffect(() => {
    fetchCustomData();
  
  },[refresh]);

      // On submitting the datas for custom editor
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
        await axios.patch(`${REACT_APP_BASE_URL}/cms-custom/${widgetId}`, {
            cms_custom_id: customId,
            widget_name: formValues.widget_name,
            widget_type: formValues.widgetTypeInput,
            cms_custom_editor: CustomContentData,
        });
        fetchWidgetOrder();
        setUnsavedChanges(false);
        fetchCustomData();
        if (DataSubmitted) {
          Swal.fire({
            title: "Success!",
            text: "Datas Updated successfully.",
            icon: "success",
            timer: 3000,
            showConfirmButton: false,
          });
        } else {
          Swal.fire({
            title: "Success!",
            text: "Datas saved successfully.",
            icon: "success",
            timer: 3000,
            showConfirmButton: false,
          });
        }
    } catch (error) {
        console.error("Error updating data:", error);
        Swal.fire("Error", "Failed to save data", "error");
    }
};
// Validation: Check for errors in the custom Block errors
const checkErrors = () => {
    let errors = {};

    // const cleanedContent = CustomContentData !== "<p><br></p>";
    if (!CustomContentData || CustomContentData ===  "<p><br></p>") {
        errors.content = "Content is required";
    }

    // Check if widget_name in formValues is empty
    if (!formValues.widget_name || formValues.widget_name.trim() === "") {
        errors.widget_name = "Widget Name is required";
    }

    setErrors(errors);
    return Object.keys(errors).length === 0;
};

 // Run validation every time the content or widget_name changes
useEffect(() => {
    checkErrors();
}, [CustomContentData,formValues.widget_name]);

  // Render component
  return (
    <>
    {!loading?<Skeleton/>: <div>
      <h1 className="text-lg  font-poppins text-center text-gray-600">
        Custom Blocks Customization
      </h1>
      <form className="space-y-4 pt-6">
      <div className="flex space-x-4">
          <div className="w-1/2  ">
            <label
              htmlFor="widget_name"
              className="block text-sm font-medium text-gray-700"
            >
              Widget Name
            </label>
            <input
              id="widget_name"
              name="widget_name"
              type="text"
              placeholder={formValues.widget_name
              }
              value={formValues.widget_name
              }
              onChange={handleChange}
              className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
            />
             <div className="mt-4">
             {errors.widget_name && (
    <span className="text-red-700 mt-6 bg-red-50 font-poppins px-2 py-1 rounded shadow-md">
        {errors.widget_name}
    </span>
)}

                </div>
          </div>
          <div className="w-1/2">
            <label
              htmlFor="widgetTypeInput"
              className="block text-sm font-medium text-gray-700"
            >
              Widget Type
            </label>
            <input
              id="widgetTypeInput"
              name="widgetTypeInput"
              type="text"
              disabled
              placeholder={formValues.widgetTypeInput}
              value={formValues.widgetTypeInput}
              className="mt-1 focus:ring-indigo-500 bg-gray-200 opacity-50 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
            />
          </div>
                {/* {customPage.content &&  <PreviewPage content={customPage.content}/>} */}
        </div>
          <label className="font-medium font-poppins pt-3 text-gray-600 block">
                  Custom Page Content
                </label>

                <SunEditor
                  setDefaultStyle="font-family: poppins; font-size: 15px;"
                  setOptions={{
                    height: 400,
                    buttonList: [
                      [
                        "font",
                        "fontSize",
                        "bold",
                        "subscript",
                        "superscript",
                        "underline",
                      ], // Added 'font'
                      ["fontColor", "hiliteColor", "outdent", "indent", "link"],
                      ["codeView", "undo", "redo", "align", "list", "table"], // Added 'codeView'
                      ["image", "video", "fullScreen", "preview"],
                    ],

                    // To define custom fonts:
                    font: [
                      "Arial",
                      "Calibri",
                      "Comic Sans",
                      "Courier",
                      "Poppins",
                      "Times New Roman",
                    ],
                    // You can add other options according to your needs
                  }}
                  setContents={CustomContentData}  // set the editor content
                  onChange={handleEditorChange}  
               
                />
                 <div className="mt-1">
                {errors.content && (
    <span className="text-red-700 mt-6 bg-red-50 font-poppins px-2 py-1 rounded shadow-md">
        {errors.content}
    </span>
)}
                </div>
                <button
            disabled={Object.keys(errors).length !== 0}
          onClick={handleSubmit}
          
          className={
            Object.keys(errors).length !== 0
              ? "text-white font-bold py-2 px-4 rounded-md opacity-50 bg-blue-500"
              :
               `bg-blue-500 hover:bg-blue-700  text-white font-bold py-2 px-4 rounded-md`
          }
        >
         {DataSubmitted ? "Update":"Save"}
         
        </button>
        </form>
    </div>}
   
    </>
  )
}

export default CmsBlock
