
// Function to determine the base URL based on the hostname
const getBaseUrl = () => {
  const hostname = window.location.hostname;
  
  // Example logic to set the base URL based on the hostname
  let baseUrl;
  if (hostname === "localhost") {
    baseUrl = "https://j3sitebuilder.ta4.in/api/v3"; // Development base URL
  } else {
    baseUrl = `https://${hostname}/api/v3`; // Production base URL
  }

  return baseUrl;
};

// Export the base URL so it can be used throughout your application
export const BASE_URL = getBaseUrl();
