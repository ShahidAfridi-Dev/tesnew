import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
// import App from './App';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
// import reportWebVitals from './reportWebVitals';
import SSL from "./Components/DomainManagement/SslCertificate";
import Gallery from "./Components/Gallery";
import TestOne from "./Components/TestOne";
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Router>
      <Routes>
        <Route path="/ssl" element={<TestOne />} />
        {/* <Route path="/gallery" element={<Gallery />} />
        <Route path="/upload" element={<Upload />} />
        <Route path="/default-gallery" element={<DefaultGallery />} />
        <Route path="/image-overlay" element={<ImageOverlay />} />
        <Route path="/godaddy" element={<GoDaddy />} />
        <Route path="/ssl" element={<SSL />} />
        <Route path="/paymentSuccess" element={<PaymentSuccess />} />
        <Route path="/paymentfailure" element={<PaymentFailure />} />
        <Route path="/test" element={<Test />} /> */}
        <Route path="/" element={<Gallery />} />
      </Routes>
    </Router>
  </React.StrictMode>
);
